Feature: Test Calculator Functionality
 Scenario: Addition
    Given I input "2"+"3" to calculator
    Then I get result "5"

 Scenario: Subtraction
   #  Given Calculator app is run 
   #  When I input "5-2" to calculator
   Given I input "5"-"2" to calculator
   Then I get result "3"

  Scenario: Multiplication
   #  Given Calculator app is run
   #  When I input "6*3" to calculator
    Given I input "6"*"3" to calculator
    Then I get result "18"

 Scenario: Division
   #  Given Calculator app is run
   #  When I input "10/2" to calculator
    Given I input "10"/"2" to calculator
    Then I get result "5.0"
 
 Scenario: Pow
   #  Given Calculator app is run
   #  When I input "2**5" to calculator
    Given I input "2"**"5" to calculator
    Then I get result "32"
