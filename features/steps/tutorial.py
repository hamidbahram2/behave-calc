from behave import given, then, use_step_matcher
from calc2 import Cal
use_step_matcher("re")

#1
#2
#3
@given('I input "2"\+"3" to calculator')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    c = Cal(2, 3)
    context.result = c.add()


@then('I get result "5"')
def step_impl(context):
    """

    :param context:
    :return:
    """
    assert context.result == 5


@given('I input "5"\-"2" to calculator')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    c = Cal(5, 2)
    context.result = c.sub()


@then('I get result "3"')
def step_impl(context):
    """

    :param context:
    :return:
    """
    assert context.result == 3


@given('I input "6"\*"3" to calculator')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """

    c = Cal(6,3)
    context.result = c.mul()


@then('I get result "18"')
def step_impl(context):
    """

    :param context:
    :return:
    """
    assert context.result == 18


@given('I input "10"\/"2" to calculator')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """

    c = Cal(10,2)
    context.result = c.div()


@then('I get result "5.0"')
def step_impl(context):
    """

    :param context:
    :return:
    """

    assert context.result == 5.0


@given('I input "2"\*\*"5" to calculator')
def step_impl(context):
    """
    :type context: behave.runner.Context
    """
    c = Cal(2,5)
    context.result = c.poow()


@then('I get result "32"')
def step_impl(context):
    """

    :param context:
    :return:
    """
    assert context.result == 32